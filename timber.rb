#!/usr/bin/env ruby
# encoding: UTF-8
#
# timber 0.3 Copyleft 2012  Vadim Kuznetsov. All Wrongs Reserved.

require 'sinatra/base'

class Timber < Sinatra::Base
  set :app_file, __FILE__
  set :root, File.expand_path(File.dirname(__FILE__))
  set :run, settings.app_file == $0
end

require_relative 'config/environment'
require_relative 'helpers/init'
require_relative 'routes/init'
require_relative 'models/init'

Timber.run! if Timber.run?