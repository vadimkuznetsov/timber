//
// messages.js
//
// Will create messages collection, if it does not exists,
//      convert to capped, if it exists and is not capped,
//      ensure, that HOST field is indexed,
//      print collection statistics
//
// Usage:
//    mongo --quiet syslog messages.js
//

// Connect to syslog db
db = connect("localhost:27017/syslog");

if( db.messages.exists()) {
  if ( !db.messages.isCapped() ) {
    db.runCommand({"convertToCapped": "messages", size:1000000, max:100000, autoIndexId:true});
    db.messages.ensureIndex({_id:1});
  }
}
else {
  db.createCollection("messages", {capped:true, size:1000000, max:100000, autoIndexId:true});
}

db.messages.ensureIndex({HOST:1});

// Print collection stats in kilobytes
printjson( db.messages.stats(1024) );
