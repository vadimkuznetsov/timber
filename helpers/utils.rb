# encoding: utf-8
#
require 'sinatra/base'
require 'haml'

module Sinatra
  module Utils
    def partial(page, options={})
      haml page, options.merge!(:layout => false)
    end
  end

  helpers Utils
end
