#encoding: utf-8
#
require 'sinatra/base'
require 'json'
require 'base64'

# Examples of the conditions as HASH:
# {:DATE => /^Jan 1\s/i }
# {:HOST => "hostname" }
# {:HOST.in => ['chi', 'phi']}
# {:PRIORITY=>{"$in"=>["err", "notice"]}}
# {:PRIORITY.in => ['err', 'notice']}
#
# as JSON
# {"DATE":{"regex":"^Jan 1\s"},"HOST":"hostname", "PPRIORITY":["crit","err"]}
#

module Sinatra
  module Conditions

      def conditions
        dou = JSON.parse(Base64.strict_decode64(request.cookies["conditions"] || 'e30='))
        query = {}
        dou.each do |key, value|
          if value.is_a?(Hash) and value.has_key?("regex")
            newValue = Regexp.new(value["regex"])
          elsif value.is_a?(Array)
            newValue = {"$in" => value}
          else
            newValue = value
          end

          case key
          when "DATE"
            query[:DATE] = newValue
          when "HOST"
            query[:HOST] = newValue
          when "FACILITY"
            query[:FACILITY] = newValue
          when "PRIORITY"
            query[:PRIORITY] = newValue
          when "MESSAGE"
            query[:MESSAGE] = newValue
          end
        end
        return query
      end

      def set_conditions(field, newValue)
        dou = JSON.parse(Base64.strict_decode64(request.cookies["conditions"] || 'e30='))
        case field
        when :DATE then dou[:DATE] = {"regex" => newValue}
        when :HOST then dou[:HOST] = newValue
        when :FACILITY then dou[:FACILITY] = newValue
        when :PRIORITY then dou[:PRIORITY] = {"$in" => [newValue]}
        when :MESSAGE then dou[:MESSAGE] = {"regex" => newValue}
        else dou = {}
        end
        response.set_cookie("conditions", {:value => Base64.strict_encode64(dou.to_json), :path => '/'})
      end

      def clear()
        response.delete_cookie "conditions", :path => '/'
      end

  end

  helpers Conditions
end

