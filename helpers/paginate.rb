# encoding: utf-8
#
require 'sinatra/base'

module Sinatra
  module Paginate
    def paginate(page, total, lines=25)
      @total = total
      @lines = lines
      @pages = @total / @lines
      @pages += 1 if @total % @lines > 0
      @pages = 1 if @total == 0
      @page = page.to_i % @pages
      @page = @pages if @page == 0
      @skip = (@page - 1) * @lines
      @prevpage = @page == 1 ? @pages : @page - 1
      @nextpage = @page == @pages ? 1 : @page + 1
      @info = "Count: #{@total} Page #{@page} of #{@pages} "
    end
  end

  helpers Paginate
end
