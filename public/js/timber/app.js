//
// app.js
//

define([
  "dojo/dom",
  "dojo/dom-construct",
  "dojo/_base/event",
  "dojo/_base/xhr",
  "dojo/cookie",
  "dojo/json",
  "dojo/on",
  "dojo/query",
  "dojo/NodeList-dom",
  "dojo/keys",
  "dijit/registry",
  "dijit/form/TextBox",
  "dijit/form/CheckBox",
  "dijit/TooltipDialog",
  "dijit/popup"
  ],
  function(dom, domConstruct, baseEvent, xhr, cookie, JSON, on, query, nodeListDom, keys, registry, TextBox, CheckBox, TooltipDialog, popup ) {
    "use strict";

    var conditions = {};

    var log = function(msg) {
      console.log(msg);
      // var timeout = setTimeout('dom.byId("info").innerHTML = "";', 5000);
      dom.byId("info").innerHTML = msg;
    };

    var fetchmsgbyid = function(id) {
      xhr.get({
        url: "/id/" + id,
        load: function(result) {
          alert(result);
        },
        error: function() {
          log("fetch error: /id/" + id);
        }
      });
    };

    var getConditions = function() {
      conditions = JSON.parse(atob(dojo.cookie("conditions") || 'e30='));
    };

    var setConditions = function() {
      dojo.cookie("conditions", btoa(JSON.stringify(conditions)), { path: '/', expires: 1 });
      log(JSON.stringify(conditions));
    };

    var addFilter = function(field) {
      query("#th_" + field.toLowerCase()).on("click", function(e) {
        var oldValue;
        if( conditions[filed] && conditions[field].regex ){
          oldValue = conditions[filed].regex;
        } else {
          oldValue = "";
        }
        var fieldTextBox = registry.byId("tb_" + field);
        var fieldDialog = registry.byId("td_" + field);
        if(fieldTextBox == null){
          fieldTextBox = new dijit.form.TextBox({
            id: "tb_" + field,
            name: "tb_" + field,
            value: oldValue,
            autofocus: true,
            trim:      true,
            onChange: function(newValue) {
              if(newValue != ""){
                conditions[field] = {"regex": newValue};
              } else {
                delete conditions[filed]
              }
              setConditions();
            },
            onKeyPress: function(evt) {
              if(evt.keyCode == keys.ENTER) {
                fieldTextBox.focusNode.blur();
              }
            },
          });
        };
        if(fieldDialog == null){
          fieldDialog = new dijit.TooltipDialog({
            id: "td_" + field,
            content: fieldTextBox,
            onBlur: function() {
              dijit.popup.close(fieldDialog);
            },
            onCancel: function() {
              dijit.popup.close(fieldDialog);
            },
            onShow: function() {
              fieldTextBox.focus();
            },
          });
        };

        dijit.popup.open({
          popup: fieldDialog,
          around: this,
        });
      });
    };

    var initUI = function() {
      getConditions();

      query("#log_table tbody tr:nth-child(odd)").addClass("odd");
      query("#log_table tbody tr:nth-child(even)").addClass("even");

      // Add pick a day filter
      query("#th_date").on("click", function(e) {
        var oldValue;
        if( conditions.DATE && conditions.DATE.regex ){
          oldValue = conditions.DATE.regex;
        } else {
          oldValue = "";
        }
        var dateTB = registry.byId("tb_date");
        var dateTD = registry.byId("td_date");
        if(dateTB == null){
         dateTB = new dijit.form.TextBox({
            id: "tb_date",
            name: "tb_date",
            value: oldValue,
            autofocus: true,
            trim:      true,
            onChange: function(newValue) {
              if(newValue != ""){
                conditions.DATE = {"regex": newValue};
              } else {
                delete conditions.DATE
              }
              setConditions();
            },
            onKeyPress: function(evt) {
              if(evt.keyCode == keys.ENTER) {
                dateTB.focusNode.blur();
              }
            },
          });
        };
        if(dateTD == null){
          dateTD = new dijit.TooltipDialog({
            id: "td_date",
            content: dateTB,
            onBlur: function() {
              dijit.popup.close(dateTD);
            },
            onCancel: function() {
              dijit.popup.close(dateTD);
            },
            onShow: function() {
              dateTB.focus();
            },
          });
        };

        dijit.popup.open({
          popup: dateTD,
          around: this,
        });
      });

      // Add host filter
      query("#th_host").on("click", function(e) {
        var oldValue;
        if( conditions.HOST ){
          if( conditions.HOST.regex ){
            oldValue = conditions.HOST.regex;
          } else {
            oldValue = conditions.HOST;
          }
        } else {
          oldValue = "";
        }
        var hostTB = registry.byId("tb_host");
        var hostTD = registry.byId("td_host");
        if(hostTB == null){
          hostTB = new dijit.form.TextBox({
            id: "tb_host",
            name: "tb_host",
            value: oldValue,
            autofocus: true,
            trim:      true,
            onChange: function(newValue) {
              if(newValue != ""){
                conditions.HOST = {"regex": newValue};
              } else {
                delete conditions.HOST
              }
              setConditions();
            },
            onKeyPress: function(evt) {
              if(evt.keyCode == keys.ENTER) {
                hostTB.focusNode.blur();
              }
            },
          });
        };
        if(hostTD == null){
          hostTD = new dijit.TooltipDialog({
            id: "td_host",
            content: hostTB,
            onBlur: function() {
              dijit.popup.close(hostTD);
            },
            onCancel: function() {
              dijit.popup.close(hostTD);
            },
            onShow: function() {
              hostTB.focus();
            },
          });
        };

        dijit.popup.open({
          popup: hostTD,
          around: this,
        });
      });

      // Add Facility filter
      query("#th_facility").on("click", function(e) {
        var oldValue;
        if( conditions.FACILITY ){
          if(conditions.FACILITY.regex ){
            oldValue = conditions.FACILITY.regex;
          } else {
            oldValue = conditions.FACILITY;
          }
        } else {
          oldValue = "";
        }
        var facilityTD = registry.byId("td_facility");
        var facilityTB = registry.byId("tb_facility");
        if(facilityTB == null){
          facilityTB = new dijit.form.TextBox({
            id: "tb_facility",
            name: "tb_facility",
            value: oldValue,
            autofocus: true,
            trim:      true,
            onChange: function(newValue) {
              if(newValue != ""){
                conditions.FACILITY = {"regex": newValue};
              } else {
                delete conditions.FACILITY
              }
              setConditions();
            },
            onKeyPress: function(evt) {
              if(evt.keyCode == keys.ENTER) {
                facilityTB.focusNode.blur();
              }
            },
          });
        };
        if(facilityTD == null){
          facilityTD = new dijit.TooltipDialog({
            id: "td_facility",
            content: facilityTB,
            onBlur: function() {
              dijit.popup.close(facilityTD);
            },
            onCancel: function() {
              dijit.popup.close(facilityTD);
            },
            onShow: function() {
              facilityTB.focus();
            },
          });
        };

        dijit.popup.open({
          popup: facilityTD,
          around: this,
        });
      });

      // Add Level filter
      query("#th_level").on("click", function(e) {
        var levelTD = registry.byId("td_level");
        var levels = [
          {id: "emergency", label: "Emergency"},
          {id: "alert",     label: "Alert"},
          {id: "crit",      label: "Critical"},
          {id: "err",       label: "Error"},
          {id: "warning",   label: "Warning"},
          {id: "notice",    label: "Notice"},
          {id: "info",      label: "Info"},
          {id: "debug",     label: "Debug"}
        ];

        if(levelTD == null){
          levelTD = new dijit.TooltipDialog({
            id: "td_level",
            content: "",
            onBlur: function() {
              var checkedLevels = [];
              query('input:checked', 'td_level').forEach(function(node){
                checkedLevels.push(node.name);
              });
              if( checkedLevels.length > 0){
                conditions.PRIORITY = checkedLevels;
              } else {
                delete conditions.PRIORITY;
              }
              setConditions();
              dijit.popup.close(levelTD);
            },
            onCancel: function() {
              dijit.popup.close(levelTD);
            },
            onShow: function() {
              levelTD.focus();
            },
          });
        };
        for (var i = 0; i < levels.length; i++) {
          levels[i].checkbox = registry.byId(levels[i].id);
          if(levels[i].checkbox == null){
            levels[i].checkbox = new dijit.form.CheckBox({
              id:       levels[i].id,
              name:     levels[i].id,
              value:    levels[i].id,
              checked:  false,
              onChange: function(newValue){
                //log(this.id + ' onChange called with parameter = ' + newValue + ', and widget value = ' + this.get('value') );
              }
            });
            //levels[i].checkbox.set('checked', true);
            domConstruct.place(levels[i].checkbox.domNode, levelTD.containerNode);
            domConstruct.place(domConstruct.toDom('<label for="' + levels[i].id +'"> ' + levels[i].label + '</label></br>'), levelTD.containerNode);
          }

        };

        dijit.popup.open({
          popup: levelTD,
          around: this,
        });
      });

      query("#th_message").on("click", function(e) {
        var oldValue;
        if( conditions.MESSAGE && conditions.MESSAGE.regex ){
          oldValue = conditions.MESSAGE.regex;
        } else {
          oldValue = "";
        }
        var messageTB = registry.byId("tb_message");
        var messageTD = registry.byId("td_message");
        if(messageTB == null){
          messageTB = new dijit.form.TextBox({
            id: "tb_message",
            name: "tb_message",
            value: oldValue,
            autofocus: true,
            trim:      true,
            onChange: function(newValue) {
              if(newValue != ""){
                conditions.MESSAGE = {"regex": newValue};
              } else {
                delete conditions.MESSAGE
              }
              setConditions();
            },
            onKeyPress: function(evt) {
              if(evt.keyCode == keys.ENTER) {
                messageTB.focusNode.blur();
              }
            },
          });
        };
        if(messageTD == null){
          messageTD = new dijit.TooltipDialog({
            id: "td_message",
            content: messageTB,
            onBlur: function() {
              dijit.popup.close(messageTD);
            },
            onCancel: function() {
              dijit.popup.close(messageTD);
            },
            onShow: function() {
              messageTB.focus();
            },
          });
        };

        dijit.popup.open({
          popup: messageTD,
          around: this,
        });
      });

      query(".message").on("click", function(e) {
        fetchmsgbyid(this.id);
      });
    };

    return {
      init: initUI
    };
});