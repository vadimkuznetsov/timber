# Folder public/js/lib #

[Download](http://download.dojotoolkit.org/release-1.8.0/dojo-release-1.8.0.tar.gz)
and extract [Dojo Toolkit](http://dojotoolkit.org/) into this folder

Or use this command:

  wget -qO- http://download.dojotoolkit.org/release-1.8.0/dojo-release-1.8.0.tar.gz | tar xzf - --strip=1