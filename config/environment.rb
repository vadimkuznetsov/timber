# encoding: UTF-8
#
require 'sinatra/base'
require 'sinatra/reloader'
require 'yaml'
require 'mongoid'

class Timber < Sinatra::Base
  configure  do
    enable :sessions

    puts "Current environment is " + settings.environment.to_s

  end

  configure :production do
    Mongoid.load!(settings.root + "/config/mongoid.yml")
  end

  configure :development do
    register Sinatra::Reloader

    Mongoid.configure do |config|
      config.master = Mongo::Connection.new.db("syslog")
    end
  end

end