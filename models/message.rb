# encoding: UTF-8
require 'mongoid'

class Message
  include Mongoid::Document

  field :DATE,     :as => :date
  field :HOST,     :as => :host
  field :FACILITY, :as => :facility
  field :PRIORITY, :as => :level
  field :MESSAGE,  :as => :message

  index :DATE
  index :HOST
end