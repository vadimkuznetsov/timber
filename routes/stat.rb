# encoding: utf-8

require 'sinatra/base'
require 'haml'

class Timber < Sinatra::Base

  get '/stat' do
    @title = "timber! - log statistics"
    haml :stat
  end
end