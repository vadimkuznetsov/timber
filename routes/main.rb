# encoding: utf-8
#
require 'sinatra/base'
require 'haml'
require 'json'

class Timber < Sinatra::Base
  helpers Sinatra::Conditions
  helpers Sinatra::Paginate
  helpers Sinatra::Utils

  get '/' do
    redirect "/page/0"
  end

  get '/all' do
    response.delete_cookie "conditions", :path => '/'
    redirect '/page/0'
  end

  get '/page/:num' do |num|
    redirect "/page/1" if num.nil?
    @messages = Message.all(:conditions => conditions)
    paginate(num, @messages.count)
    @title = "timber!"
    @info = "#{@messages.selector}"
    haml :index
  end

  get '/today' do
    today = Date.today.strftime("%b %e")
    set_conditions :DATE, "^#{today}\s"
    redirect "/page/0"
  end

  get '/yesterday' do
    yesterday = Date.yesterday.strftime("%b %e")
    set_conditions :DATE, "^#{yesterday}\s"
    redirect "/page/0"
  end

  get '/host/:host' do
    set_conditions :HOST, params[:host]
    redirect "/page/0"
  end

  get '/facility/:facility' do
    set_conditions  :FACILITY, params[:facility]
    redirect "/page/0"
  end

  get '/priority/:level' do
    set_conditions :PRIORITY, params[:level]
    redirect "/page/0"
  end

  get '/id/:id' do |id|
    @messages = Message.where(:_id => id)
    paginate(1, @messages.count)
    @info = @messages.selector
    if request.xhr?
      @messages.to_json
    else
      haml :index
    end
  end
end