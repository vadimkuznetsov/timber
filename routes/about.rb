# encoding: utf-8

require 'sinatra/base'
require 'haml'

class Timber < Sinatra::Base

  get '/about' do
    @title = "timber! - about"
    @info = ""
    haml :about
  end
end
