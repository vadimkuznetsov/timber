# encoding: utf-8

require 'sinatra/base'
require 'sass'

class Timber < Sinatra::Base
  get '/screen.css' do
    content_type 'text/css', :charset => 'utf-8'
    sass :screen
  end
end